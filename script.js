var players = {
    red: {},
    blue: {},
    current: "red"
};

$("div[val~='one']").on("click", function () {
    addPoint("one");
});
$("div[val~='two']").on("click", function () {
    addPoint("two");
});
$("div[val~='three']").on("click", function () {
    addPoint("three");
});
$("div[val~='four']").on("click", function () {
    addPoint("four");
});
$("div[val~='five']").on("click", function () {
    addPoint("five");
});
$("div[val~='six']").on("click", function () {
    addPoint("six");
});
$("div[val~='seven']").on("click", function () {
    addPoint("seven");
});
$("div[val~='eight']").on("click", function () {
    addPoint("eight");
});

function addPoint(number) {
    if (players.current === "red") {
        if (!players.red[number]) {
            players.red[number] = 1;
        }
        else {
            players.red[number] += 1;
        }
    }
    else {
        if (!players.blue[number]) {
            players.blue[number] = 1;
        }
        else {
            players.blue[number] += 1;
        }
    }
}
function checkIfWin(player) {
    for (var group in players[player]) {
        if (players[player][group] === 3) {
            gameWon();
        }
    }
}
function gameWon() {
    $("body").css("background-color", "black");
    $("body").css("opacity", "0.5");
}

$(".box").on("click", function () {
    $(this).css('background-color', players.current);
    checkIfWin(players.current);
    if (players.current === "red") {
        players.current = "blue";
    }
    else {
        players.current = "red";
    }
});